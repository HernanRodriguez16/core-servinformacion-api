package com.puntored.servinformacion.dto;

/**
 *
 * @author Brainwinner
 */
public class RequestServinformacionDTO {

    String proceso;
    String identificador;
    String cuerpo;

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getCuerpo() {
        return cuerpo;
    }

    public void setCuerpo(String cuerpo) {
        this.cuerpo = cuerpo;
    }
}
