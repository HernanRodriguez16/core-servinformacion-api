package com.puntored.servinformacion.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Brainwinner
 */
public class VerticaConnection {

    public static PropertiesBD propiedades = new PropertiesBD();
    public static InputStream entrada = null;
    private static Connection cnx = null;

  
    public static Connection getConnection() throws SQLException, ClassNotFoundException, FileNotFoundException, IOException {
        cnx = null;
       
       
        if (cnx == null) {
            try {
                System.out.println("inicia conexion a Vertica");
                Class.forName(propiedades.getProperty("bwg.jdbc.driver"));
                System.out.println("driver: " + propiedades.getProperty("bwg.jdbc.driver"));
                System.out.println("url: " + propiedades.getProperty("bwg.jdbc.url"));
                System.out.println("usuario: " + propiedades.getProperty("bwg.jdbc.user"));
                System.out.println("password: " + propiedades.getProperty("bwg.jdbc.password"));

                cnx = DriverManager.getConnection(propiedades.getProperty("bwg.jdbc.url"), propiedades.getProperty("bwg.jdbc.user"), propiedades.getProperty("bwg.jdbc.password"));

                System.out.println("Conexion Exitosa");
            } catch (SQLException ex) {
                System.out.println("excepcion sql: "+ex);
                throw new SQLException(ex);
            }
        }
        return cnx;
    }
}
