package com.puntored.servinformacion.connection;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;
/**
 *
 * @author Hernan Rodriguez
 */

public class PropertiesBD {
    protected Properties props = null;

    public PropertiesBD() {
        FileInputStream in = null;
        try {
            
            props = new Properties();
    //        in = new FileInputStream("C:\\Users\\Wilmer Alzate\\Documents\\NetBeansProjects\\core-servinformacion-api\\src\\com\\puntored\\servinformacion\\connection\\vertica-config.properties");
              in = new FileInputStream("/opt/servinformacion/vertica-config.properties");
            props.load(in);
            in.close();
           
        } catch (Exception e) {
            Logger.getAnonymousLogger().info("Error en la ruta del archivo:" + e.getMessage());
            try {
                InputStream inn = null;
                props = new Properties();
                inn = this.getClass().getResourceAsStream("/com/puntored/servinformacion/connection/vertica-config.properties");
                props.load(inn);
                inn.close();
                Logger.getAnonymousLogger().info("LOADED \"/com/puntored/connection/vertica-config.properties");
            } catch (Exception ex) {
                Logger.getAnonymousLogger().severe("ERROR LOADING FILE:" + ex.getMessage());
            }

        }
    }
    public String getProperty(String name) {
        try {            
            return props.getProperty(name);
        } catch (Exception e) {
            Logger.getAnonymousLogger().severe("PROPERTY ERROR:" + e.getMessage());
        }
        return null;
    }
}