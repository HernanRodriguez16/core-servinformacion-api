package com.puntored.servinformacion.client;

import com.brainwinner.dto.ProviderDTO;
import com.brainwinner.efecty.bigdata.protocol.channel.EfectyChannel;
import com.brainwinner.efecty.bigdata.protocol.packager.EfectyDataPackager;
import com.brainwinner.util.LogSocketListener;
import com.puntored.servinformacion.dto.RequestServinformacionDTO;
import com.puntored.servinformacion.dto.ResponseConsultaDTO;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;
import org.jpos.util.ThreadPool;

/**
 *
 * @author Hernan Rodriguez
 */
public class ServinformacionCoreClient implements IClientChannel {

    private ProviderDTO providerDTO;
    private EfectyChannel channel;
    private static String logName = "SERVINFORMACION_CORE_channel";
    private static LogSocketListener logger;

    protected Properties props = null;
    private FileInputStream in = null;

    public ServinformacionCoreClient(ProviderDTO providerDTO) {
        System.out.println("Aqui");
        this.providerDTO = providerDTO;

        ServinformacionCoreClient.getLogger();

    }

    public static void getLogger() {
        if (logger == null) {
            PrintStream out = null;
            SimpleDateFormat sdf = null;
            ThreadPool pool = null;
            try {
                sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
                out = new PrintStream(new FileOutputStream("/var/local/core/logs/" + logName + "_" + sdf.format(new Date()) + ".log"));
                logger = new LogSocketListener(out);
                System.out.println("log created");
            } catch (FileNotFoundException ex) {
                System.out.println("can not create log");
                ex.printStackTrace();
            }
        }

    }

    @Override
    public boolean conectar() throws IOException, SocketException {
        System.out.println("Conectando..");
        boolean estado = false;
        try {

            Logger logger = new Logger();
            logger.addListener(new SimpleLogListener(ServinformacionCoreClient.logger.getPrintStream()));

            channel = new EfectyChannel(this.providerDTO.getServidor(), this.providerDTO.getPuerto(), new EfectyDataPackager());
            channel.setTimeout(this.providerDTO.getTimeOut());
            channel.setLogger(logger, logName);
            channel.connect();
            System.out.println("Conectado al core de servinformacion");
            estado = true;
        } catch (Exception e) {
            System.out.println("No se puede conectar con el core de servinformacion");
            e.printStackTrace();
        }
        return estado;
    }

    @Override
    public void enviar(RequestServinformacionDTO request) throws IOException {

        ISOMsg msg = null;
        String proceso = null;
        String identificador = null;
        String cuerpo = null;

        try {

            logger.log("Enviando del core...");
            if (channel.isConnected()) {

                logger.log("Canal Conectado");
                proceso = request.getProceso();
                identificador = request.getIdentificador();
                cuerpo = request.getCuerpo();

                msg = new ISOMsg();
                msg.setMTI("0220");
                msg.set(3, proceso);
                msg.set(5, identificador);
                msg.set(61, cuerpo);

                logger.log("Proceso: " + proceso + " Identificador: " + identificador + " Cuerpo: " + cuerpo);
//                msg.dump(System.out, "Sending");
                channel.send(msg);
            } else {
                logger.log("channel can not connected with servinformacion core");
            }
        } catch (NumberFormatException e) {
            System.out.println("failed to send from core NUMBER: " + e);
            e.printStackTrace(logger.getPrintStream());
        } catch (ISOException e) {
            System.out.println("failed to send from core ISO: " + e);
            e.printStackTrace(logger.getPrintStream());
        } catch (IOException e) {
            System.out.println("failed to send from core IO: " + e);
            e.printStackTrace(logger.getPrintStream());
        }
    }

    @Override
    public ResponseConsultaDTO recibir() throws IOException {
        ResponseConsultaDTO respuesta = new ResponseConsultaDTO();
        ISOMsg msg = null;
        byte[] bytes = null;
        try {
            logger.log("Obteniendo respuesta de core Servinformacion");

            if (channel.isConnected()) {
                msg = channel.receive();
     //           msg.dump(System.out, "Return");    Para eliminar impresion en el log core.

                if (msg.getString(39).equals("00")) {
                    respuesta.setSelect(new String((byte[]) msg.getValue(121)));
                    respuesta.setEstado("OK");
                    System.out.println("asignacion correcta campor 39? " + respuesta.getEstado());

                } else {
                    respuesta.setSelect(new String((byte[]) msg.getValue(121)));
                    respuesta.setEstado("KO");
                }
            } else {
                System.out.println("Error de conexion con Core de Servinformacion");
                logger.log("Error de conexion con Core de Servinformacion");
            }
        } catch (IOException e) {
            System.out.println("error recibiendo de Core de Servinformacion: " + e);
            e.printStackTrace(logger.getPrintStream());
        } catch (ISOException e) {
            System.out.println("error recibiendo ISO de core Servinformacion: " + e);
            e.printStackTrace(logger.getPrintStream());
        } finally {
            this.disconnect();
        }
        return respuesta;
    }

    @Override
    public void disconnect() throws IOException {
        try {
            if (channel != null) {
                channel.disconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
