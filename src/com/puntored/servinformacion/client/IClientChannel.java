/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.servinformacion.client;

import com.puntored.servinformacion.dto.RequestServinformacionDTO;
import com.puntored.servinformacion.dto.ResponseConsultaDTO;
import com.puntored.servinformacion.dto.ResponseServinformacionDTO;
import java.io.IOException;
import java.net.SocketException;
import org.jpos.iso.ISOMsg;

/**
 *
 * @author Brainwinner
 */
public interface IClientChannel {

    public boolean conectar() throws IOException, SocketException;

    public void enviar(RequestServinformacionDTO request ) throws IOException;

    public ResponseConsultaDTO recibir() throws IOException;

    public void disconnect() throws IOException;

}
