package com.puntored.servinformacion.formatter;

import com.brainwinner.connection.BWGConnection;
import com.brainwinner.dto.ProviderDTO;
import com.brainwinner.util.IFormatter;
import com.brainwinner.util.LogSocketListener;
import com.brainwinner.util.ObjectDynamicCaller;
import com.brainwinner.util.ReadPropertiesFile;
import com.puntored.servinformacion.client.IClientChannel;
import com.puntored.servinformacion.dto.RequestServinformacionDTO;
import com.puntored.servinformacion.dto.ResponseConsultaDTO;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
/**
 *
 * @author Hernan Rodriguez
 */
public class ConsultaDatosFormatter implements IFormatter {

    BWGConnection aCon = null;
    private static ReadPropertiesFile prop = ReadPropertiesFile.getInstance();
    private static String prefix = prop.getProperty("prefix");
    private static LogSocketListener log = null;
    private final static String LOG_NAME = "CONSULTA_SERVINFORMACION";
    static ReadPropertiesFile pro = ReadPropertiesFile.getInstance();

    public ConsultaDatosFormatter(BWGConnection con) {
        this.aCon = con;
        System.out.println("entra formater");
        getLog();
    }

    private static LogSocketListener getLog() {
        if (log == null) {
            PrintStream out = null;
            SimpleDateFormat sdf = null;
            try {
                sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
                out = new PrintStream(new FileOutputStream("/var/local/core/logs/" + LOG_NAME + "_" + sdf.format(new Date()) + ".log"));
                log = new LogSocketListener(out);
            } catch (Exception e) {
                log = new LogSocketListener();
                e.printStackTrace();
            }
        }
        return log;
    }

    @Override
    public ISOMsg erouted(ISOMsg msg) {
        byte[] bytes = null;
        ISOMsg response = null;
        ResponseConsultaDTO respuesta = null;
        ProviderDTO providerDTO = null;
        IClientChannel channel = null;
        ObjectDynamicCaller caller = new ObjectDynamicCaller();
        RequestServinformacionDTO request = null;
        int estado = 0;

        double valor = 0d;

        try {
            response = (ISOMsg) msg.clone();
            response.setResponseMTI();
            request = new RequestServinformacionDTO();
            request.setProceso(msg.getString(3));
            request.setIdentificador(msg.getString(5));
            request.setCuerpo(msg.getString(61));

            providerDTO = new ProviderDTO();
            providerDTO.setPuerto(Integer.parseInt(pro.getProperty("servinformacion.port"))); //8024 puerto del core
            providerDTO.setServidor(pro.getProperty("servinformacion.host")); //192.168.3.26 ip core
            providerDTO.setClassChannel(pro.getProperty("servinformacion.classchannel")); // clase del cliente core servinformacion
            providerDTO.setTimeOut(Integer.parseInt(pro.getProperty("servinformacion.timeout")));  // timeout servinformacion

            System.out.println("request a servinformacion core: " + request.getProceso() + " - " + request.getIdentificador() + " - " + request.getCuerpo());

            if (providerDTO != null) {
                channel = (IClientChannel) caller.getClientChannel(providerDTO.getClassChannel(), providerDTO);
                getLog().log("channel " + channel.toString());

                if (channel != null) {
                    if (channel.conectar()) {
                        System.out.println("Canal conectado: ");
                        channel.enviar(request);
                        System.out.println("Recibiendo respuesta de core intermedio: ");
                        respuesta = channel.recibir();
                        bytes = respuesta.getSelect().getBytes();
                        response.set(39, respuesta.getEstado());
                        response.set(121, bytes);

                    } else {
                        System.out.println("ERROR DE CONEXION CON  SERVINFORMACION");
                        getLog().log("ERROR DE CONEXION CON SERVINFORMACION");
                        //      response.set(new ISOField(123, "ERROR DE CONEXION CON SERVINFORMACION"));
                    }
                } else {
                    System.out.println("ERROR AL CONECTAR AL CANAL DE SERVINFORMACION - CANAL NULO");
                    getLog().log("channel null");
                }
            } else {
            }
        } catch (IOException ex) {
            Logger.getLogger(ConsultaDatosFormatter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ISOException ex) {
            Logger.getLogger(ConsultaDatosFormatter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return response;
    }
}