/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.servinformacion.service;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonInitException;

/**
 *
 * @author Brainwinner
 */
public class ServinformacionService implements Daemon {
    
    private BWGServer server = null;

    @Override
    public void init(DaemonContext dc) throws DaemonInitException, Exception {
        System.out.println("starting servinformacion core service");
    }

    @Override
    public void start() throws Exception {
        System.out.println("Starting servinformacion service at port: " + System.getProperty("config.port"));
        server = new BWGServer(Integer.parseInt(System.getProperty("config.port")), new ListenerServinformacion());
        server.init();
    }

    @Override
    public void stop() throws Exception {
        System.out.println("stoping servinformacion core service");
        if (this.server != null) {
            this.server.stop();
        }
    }

    @Override
    public void destroy() {
        System.out.println("Finishing servinformacion core service");
    }
    
}
