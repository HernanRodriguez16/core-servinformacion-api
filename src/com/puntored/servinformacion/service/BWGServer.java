/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puntored.servinformacion.service;

import com.brainwinner.efecty.bigdata.protocol.channel.EfectyChannel;
import com.brainwinner.efecty.bigdata.protocol.packager.EfectyDataPackager;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOServer;
import org.jpos.iso.ServerChannel;
import org.jpos.util.Logger;
import org.jpos.util.SimpleLogListener;
import org.jpos.util.ThreadPool;

/**
 *
 * @author Brainwinner
 */
public class BWGServer {

    private static ISOServer server = null;
    private static Thread proceso = null;
    private static int puerto = 0;
    private static ISORequestListener listener = null;

    public BWGServer(int port, ISORequestListener listener) {
        this.puerto = port;
        this.listener = listener;
    }

    public static void init() {
        PrintStream out = null;
        SimpleDateFormat sdf = null;
        try {
            Logger logger = new Logger();
            ISOChannel clientSideChannel = new EfectyChannel(new EfectyDataPackager());
            System.out.println("clientSideChannel: "+clientSideChannel);
            ThreadPool pool = null;
            sdf = new SimpleDateFormat("yyyyMMdd-HHmm");
            out = new PrintStream(new FileOutputStream("/var/log/" + System.getProperty("config.service") + "_" + sdf.format(new Date()) + ".log"));
            System.out.println("out: "+out);
            logger.addListener(new SimpleLogListener(out));

            pool = new ThreadPool(100, 200);
            pool.setLogger(logger, "iso-pool-servinformacion");
            server = new ISOServer(puerto, (ServerChannel) clientSideChannel, pool);
            System.out.println("server: "+server);
            server.setLogger(logger, "iso-server-servinformacion");
            server.addISORequestListener(listener);
            System.out.println("server: "+server);
            proceso = new Thread(server);
            System.out.println("proceso: "+proceso);
            proceso.start();
            System.out.println("Start?? : "+proceso.getState());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void stop() {
        if(server != null){
            server.shutdown();
        }
    }

}
