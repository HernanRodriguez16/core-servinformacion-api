package com.puntored.servinformacion.service;

import com.puntored.servinformacion.connection.VerticaConnection;
import com.puntored.servinformacion.dto.ResponseServinformacionDTO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISORequestListener;
import org.jpos.iso.ISOSource;
import org.jpos.util.ThreadPool;

/**
 *
 * @author Hernan Rodriguez
 */
public class ListenerServinformacion implements ISORequestListener {

    private static final int minThread = 100;
    private static final int maxThread = 200;
    private final ThreadPool poolMessage = new ThreadPool(minThread, maxThread);
    private static int process = 0;
    private static int processed = 0;
    private static int errors = 0;

    private VerticaConnection vCon = null;

    public ListenerServinformacion() {

        System.out.println("Entrada Listener constructor vacio");

    }

    @Override
    public boolean process(ISOSource isosrc, ISOMsg isomsg) {
        boolean estado = false;
        try {
            poolMessage.execute(new Process(isosrc, isomsg));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return estado;
    }

    class Process implements Runnable {

        private ISOSource source;
        private ISOMsg msg;

        public Process(ISOSource source, ISOMsg msg) {
            this.source = source;
            this.msg = msg;
        }

        @Override
        public void run() {
            System.out.println("runing servinformacion listener thread!");
            ISOMsg response = null;

            String proceso = "";
            String identificador = "";
            String cuerpo = "";
            try {
                process++;
                response = (ISOMsg) this.msg.clone();
                response.setResponseMTI();
                System.out.println(new Date() + " INICIANDO : MTI[" + msg.getMTI() + "] PROCESO: " + msg.getString(3) + " IDENTIFICADOR: " + msg.getString(5) + " FILTRO: " + msg.getString(61));
                proceso = msg.getString(3);

                Connection con = null;
                PreparedStatement ps = null;
                ResultSet rs = null;
                String respuesta = null;
                byte[] bytes = null;
                StringBuilder json = new StringBuilder();
                json.append("{\"select\": [");

                ResponseServinformacionDTO objeto = null;
                List<ResponseServinformacionDTO> select = null;

                switch (proceso) {
                    case "250260": {
                        try {
                            identificador = this.msg.getString(5);
                            cuerpo = this.msg.getString(61);
                            String consulta = null;
                            String estructura = null;
                            VerticaConnection vCon = new VerticaConnection();

                            System.out.println("data: " + identificador + " - " + cuerpo);
                            con = vCon.getConnection();
                            ps = con.prepareStatement("SELECT consulta,estructura FROM proveedores.webservice where id =" + identificador);
                            rs = ps.executeQuery();
                            if (rs.next()) {
                                if (cuerpo.equals("0")) {
                                    consulta = rs.getString("consulta");
                                } else {
                                    consulta = rs.getString("consulta") + cuerpo;
                                }
                                estructura = rs.getString("estructura");

                                if (consulta != null) {
                                    String[] campos = estructura.split(",");
                                    String arreglo = null;
                                    ps = con.prepareStatement(consulta);
                                    System.out.println("inicia proceso segunda consulta");
                                    rs = ps.executeQuery();
                                    while (rs.next()) {
                                        json.append("{");
                                        for (String campo : campos) {
                                            System.out.println("campo: " + campo + " " + rs.getString(campo));
                                            json.append("\"" + campo + "\"" + " : " + "\"" + rs.getString(campo) + "\"" + ",");
                                        }
                                        json.append("},");
                                    }
                                    respuesta = json.toString().substring(0, json.length() - 1).replace(",}", "}").concat("]");// .substring(0,json.length()-1);

                                    try {
                                        if (rs != null) {
                                            rs.close();
                                            System.out.println("cierra rs");
                                        }
                                        rs = null;
                                        if (ps != null) {
                                            ps.close();
                                            System.out.println("cierra ps");
                                        }
                                        ps = null;
                                        if (con != null) {
                                            con.close();
                                            System.out.println("cierra rs");
                                        }
                                        con = null;
                                    } catch (SQLException ex) {
                                        ex.printStackTrace();
                                    }
                                    bytes = respuesta.getBytes();

                                    response.set(121, bytes);
                                    response.set(39, "00");

                                    response.set(63, "consulta exitosa");

                                } else {
                                    System.out.println("Error al procesar la consulta con filtro");
                                    response.set(39, "99");
                                    response.set(63, "Error al procesar la consulta con filtro");
                                }
                                System.out.println("finaliza consulta");

                            } else {
                                System.out.println("No se encontro la consulta");
                                response.set(39, "99");
                                response.set(63, "Consulta no encontrada");
                            }

                        } catch (Exception e) {
                            json.append("{ \"error\": \"error realizando proceso de consulta a la BD\"}]");
                            System.out.println("error realizando proceso de consulta a la BD: "+json);
                            respuesta = json.toString();
                            bytes = respuesta.getBytes();
                            response.set(121, bytes);
                            response.set(39, "99");
                            response.set(63, "Error en el filtro de la consulta: ");

                        } finally {
                            try {
                                if (rs != null) {
                                    rs.close();
                                }
                                rs = null;
                                if (ps != null) {
                                    ps.close();
                                }
                                ps = null;
                                if (con != null) {
                                    con.close();
                                }
                                con = null;
                            } catch (SQLException ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                    process--;
                    processed++;

                    source.send(response);
                    System.out.println(new Date() + " FINALIZA PROCESAMIENTO DE SERVINFORMACION");
                }
            } catch (IOException ex) {
                Logger.getLogger(ListenerServinformacion.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ISOException ex) {
                Logger.getLogger(ListenerServinformacion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
